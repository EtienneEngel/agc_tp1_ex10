## Aim of this repository

Drop the functional part of a package on a pypi repository

## Context

The Python foundation, via PEP427, defines an official format: the wheel.

## Command to write in a linux terminal

```ruby
python setup.py bdist_wheel

pip install twine pyopenssl ndg-httpsclient pyasn1

python setup.py register

twine upload dist/TestSimpleCalculatorEtienneEngel-0.0.2-py3-none-any.whl

```

## Check the package is really online

Go to the url provide by the linux terminal after the last command line:

https://pypi.org/project/TestSimpleCalculatorEtienneEngel/0.0.2/

## Install the package from online to local

```ruby
pip install TestSimpleCalculatorEtienneEngel
```

To verify that the package is really download:

```ruby
pip freeze | grep Test
```

And the package normally shows up.
